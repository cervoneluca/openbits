# OpenBits

<div align="center">
   <img align="center" src="assets/logo-black-new.svg" width="350px" alt="OpenBits logo" title="OpenBits Logo">
</div>

OpenBits is a platform based on the [Arweave](https://www.arweave.org) blockchain. Arweave is a network that incentivize users to create censorship-resistant permanent contents. **OpenBits incentivize developers to create those small packages that lay the foundation to all the complex applications we use day by day.**

<h3>The OpenBits Web Site and Web App is here: [http://openbits.world](http://openbits.world) (also available on the IPFS [https://openbits.eth.link](https://openbits.eth.link))</h3>
<h3>The OpenBits CLI is published on NPM: [https://www.npmjs.com/package/openbits](https://www.npmjs.com/package/openbits)</h3>

<h5>The Web Site and Web Application Code Repository is here : [https://gitlab.com/cervoneluca/openbits-web](https://gitlab.com/cervoneluca/openbits-web)</h5>
<h5>The CLI Code Repository is here : [https://gitlab.com/cervoneluca/openbits-cli](https://gitlab.com/cervoneluca/openbits-cli)</h5>

## Why OpenBits?

Open Source (hereinafter OS) developers create software on a daily base. While their main target is usually to create full-fledged applications that can handle complex tasks in specific scenarios, several OS developers often create (and use) small software (i.e. packages) aimed to simplify their life (or the life of other developers) when facing common and very specific development tasks. A very significative example is the [MomentJs](https://momentjs.com/) package, that is so much useful that it is [downloaded ~12M times each week](https://www.npmjs.com/package/moment). 

> **As a result, these small software are the essential bricks that are used to build all kind of applications that we use day by day. Thus, I like to think about them as the Bits of all software and, because their source code is almost always open, they are Open Bits!**

Very often, these software are published on centralized package managers (hereinafter PMs) for the specific programming language they are written in. For instance, [NPM](https://npmjs.com) is the PM for the NODE programming language, while [Cargo](https://github.com/rust-lang/cargo) is the PM for the RUST programming language. Other more modern programming languages, like [DENO](https://deno.land/), allows developer to publish their packages wherever they want to, so they relies on decentralized PMs. 

On the one hand, PMs are very essential tools, because they collect all packages in one place, thus they make very simple to find packages one may need. Also, usually, business models of PMs supply free plans, so OS developers can publish their packages for free and make them immediately available to the OS community (for free).

On the other hand, no matter if they are centralized or they are decentralized, nowadays these PMs do not incentivize neither OS packages creators, nor developers that usually use OS packages, meaning that when a package is published and served by means of a PMs, there is not a simple and plain way to generate profit from that package.

Thus, OpenBits is aimed to supply an answer to the following questions: 

1. Is there a way to allow OS packages developers to monetize immediately their packages? 
2. Even if packages creators and packages users must be always allowed to publish or install packages for free, is there a way to incentivize developers to pay for publishing or for using packages under specific conditions?
3. Is there a way to allow OpenBits itself to share its own profits?

> **OpenBits addresses the above issues by allowing OS packages developers to monetize them immediately by selling shares of packages to investors or to other developers. Moreover, the more developers publish packages and use packages developed by other developers, the more the community receives shares of OpenBits itself!**


## How OpenBits Works? 

OpenBits allows OS creators to publish their packages by paying a small fee (0.001 AR, that is the arweave crypto currency) and then, when other developers install published packages, they pay a small fee too (0.02 in AR).

Installation fees are halved between the OpenBits owners and the publisher of the package. So supposing that you create a package and that you publish it, you'll pay 1 AR (plus eventual fees owed to the arweave network for the transaction). If you need a package, and you install it, you pay 0.02 AR, so you give 0.01 AR to OpenBits and you give 0.01 AR to owners of the package that you are installing. 

Moreover, the more OpenBits is adopted, the more users become owners of OpenBits! I will explain better how this work (and why this matter) later in this section. But lets focus on creators of packages and users of packages right now. 

A problem arises. Why developers should choose to pay 0.01 AR to install a package, and why they should choose to pay 1 AR to publish a package while, usually, PMs allow to publish and download packages for free? 

**Firstly, and very importantly, OpenBits does not aim to replace other PMs. OS creators and developers should continue to rely on PMs they are used to, and so, they must be allowed to publish or install packages for free.**

> **OpenBits is only aimed to be a parallel and not-mandatory-to-use PM that supplies some engaging features!** 

Thus, secondly, **if developers choose to use OpenBits too**, they both could make profit immediately by means of their OS works, and they contribute to speed up the process of making specific software owned by the whole community of OpenBits' users. 

I define this kind ok economy as **Capitalism By Vaccination** (hereinafter CBVAC).

When an OpenBit is published, creators of it define a maximum amount of profit they would like to earn. As more the OpenBit is download by others developers, as more shares of the OpenBit are released and assigned to OpenBits users (all of them, not only the ones that uses the specific package).

This means that, after having reached the profit target, the original creators of the packages will own the 1% of it, and they will own it forever, and the community will own the 99% of the package, and they will own it forever! 

They only exception is for people that invest early in an OpenBit. Indeed, packages creators could like to sell shares of a packages very early, in order to earn a small immediate profit. And, of course, there could be investors that would like to buy those shares if they expect to have a proper Return On Investment (hereinafter ROI) from their investment. To this extent, OS software creators may allocate a certain percentage of the shares for investors, and investors can buy them as long as packages creators still owns shares of their package. Also tho make this financial system more engaging and to make it accessible both to small and big investors, OpenBits shares are usually very cheap and can be bought only before the OpenBit generates a few profit.

To avoid that finance whales attempt to poison this shares distribution, OpenBits will restrict the amount of shares that can be allocated to investors.

Thus, In its first version, OpenBits will allow packages to be owned only by just one creator, and creators will be allowed to choose to do not allow investment, or to allow 4 investors to buy 5% shares for each. So, supposing that an OpenBit as 4 investors, when the initial state of shares will be the following:

- 80% to the creator of the OpenBit
- 5% to investor one
- 5% to investor two
- 5% to investor three
- 5% to investor four

Every time the OpenBit is used a certain amount of times (i.e. a node package is installed a certain amount of times), a 1% of shares will be transferred from the owner (and lately from investors) to a global user called the "MULTIVERSE" user. Firstly there will be transferred to the MULTIVERSE shares of owners and creators of the OpenBit, and then, there will be transferred to the MULTIVERSE shares of investors, by starting by the last investor that entered. 

Investments are leveled, meaning that they are available for a certain amount of time, and so before the OpenBit has generated a certain amount of profit, and they are different in price that investors will pay for shares. Investment levels are the following:

- **level 1 (yellow):** 
   - 5% of shares of the OpenBit are sold to the investor
   - shares will cost to the investor *0.1* of the normal price of shares
   - available before the OpenBit has generated the 2% of the target profit
- **level 2 (red):** 
   - 5% of shares of the OpenBit are sold to the investor
   - shares will cost to the investor *0.14285714285714285* of the normal price of shares
   - available before the OpenBit has generated the 3.4% of the target profit
- **level 3 (green):** 
   - 5% of shares of the OpenBit are sold to the investor
   - shares will cost to the investor *0.2* of the normal price of shares
   - available before the OpenBit has generated the 4.4% of the target profit
- **level 4 (blue):** 
   - 5% of shares of the OpenBit are sold to the investor
   - shares will cost to the investor *0.3333333333333333* of the normal price of shares
   - available before the OpenBit has generated the 5% of the target profit

The shares of the investors that bought the level 4 will be the first to be "re-bought" from the community, and so the investor will be the first to be repaid for the investment.

So, supposing that a share of an OpenBit is released and transferred to the MULTIVERSE every 10 installations of the OpenBit, after 100 installations the shares state will be the following:

- 70% to the creator of the OpenBit
- 5% to investor one
- 5% to investor two
- 5% to investor three
- 5% to investor four
- 10% to the MULTIVERSE

While, after 700 installations the state will be the following:

- 1% to the creator of the OpenBit
- 5% to investor one
- 5% to investor two
- 5% to investor three
- 4% to investor four
- 80% to the MULTIVERSE

After 1200 installation the state will be the following: 

- 1% to the creator if the OpenBit
- 5% to investor one
- 5% to investor two
- 4% to investor three
- 85% to the MULTIVERSE

In the end the shares situation will be the following (and it will last forever): 

- 1% to the creator if the OpenBit
- 99% to the MULTIVERSE

The MULTIVERSE is a placeholder user, that in a specific time represent a specific user of the OpenBit (i.e. someone that has installed the OpenBit). When a user is raffled off to be to MULTIVERSE, he will earn profits from the OpenBit for a total of the price of one share of the OpenBit. So, in the above example, where a share of the OpenBit was released each 10 downloads, a user will be the MULTIVERSE until she earn 0.01 AR for ten times (0.10). After having reached that profit, the user is put in a waiting list, and other draw is made to elect the next MULTIVERSE. When all the users have earned a profit from the OpenBit, they are all removed from the waiting list and they become eligible again to become MULTIVERSE. And this game lasts forever.

> **The amazing thing is that the same profit sharing strategy is used by OpenBits itself too. As more as OpenBits will be adopted, as more its shares will be given to the OpenBits community.**

The following section is aimed to depict a sample scenario that should clarify the functioning of OpenBits and the financial model that it implements.
           
### A Sample Scenario

Suppose that you want to publish the package "BATMANISE", that is a very simple package that given a string returns the same string with the "BAT-" prefix (for instance, given MOBILE, it returns BAT-MOBILE).
You think that a fair profit that the BATMANISE can generate, before donating it to the world, is 675 AR. So, because BATMANISE is divided into 100 shares, and because you will earn 0.01 AR for each installation of BATMANISE, you have:

- **Target Profit:** 675 AR
- **Price for each share**: 6.75 AR
- **Number of installations to reach the target profit:** 67500
- **Number of installations to release a share to the MULTIVERSE:** 675

You choose to allow investors to buy shares of BATMANISE very early, so you can have a small profit very early by selling off shares for a convenient price for investors, but only before you earn a certain profit (and so, only before the OpenBit is installed a certain amount of times). Thus, the following table describe the investments available for BATMANISE:

| Level  | Available only before you earn | Number of shares that you will sell | Price for each share that you will sell to the investor | Total price that the investor will pay to you | Expected ROI for the investor |
|--------|--------------------------------|-------------------------------------|---------------------------------------------------------|-----------------------------------------------|-------------------------------|
| YELLOW | 13.5 AR                        | 5                                   | 0.675 AR                                                | 3.375 AR                                      | 30.375 AR                     |
|   RED  | 22.95 AR                       | 5                                   | 0.9642857142857143 AR                                   | 4.821428571428571 AR                          | 28.92857142857143 AR          |
|  GREEN | 29.7 AR                        | 5                                   | 1.35 AR                                                 | 6.75 AR                                       | 27 AR                         |
|  BLUE  | 33.75 AR                       | 5                                   | 2.25 AR                                                 | 11.25 AR                                      | 22.5 AR                       |


Investors buy the shares packages YELLOW, RED and GREEN, respectively when you have earned 13 AR (at 1300 installations), 17 AR (at 1700 installations) and 25 AR (at 2500 installation). So, this means that, when BATMANISE is installed 2500 times, you have the following shares and earnings/loss state: 

|                     | owned shares | earnings/losses                                                    |
|---------------------|--------------|--------------------------------------------------------------------|
| owner               | 82           | 3.375 + 4.821428571428571 + 6.75 + (2500 x 0.01) = +39.94642857143 |
| investor 1 (yellow) | 5            | -3.375                                                             |
| investor 2 (red)    | 5            | -4.821428571428571                                                 |
| investor 3 (green)  | 5            | -6.75                                                              |
| MULTIVERSE                | 3            | 0                                                                  |

The more BATMANISE is installed, the more its shares are transferred to the MULTIVERSE so, for instance, lets say that BATMANISE is installed 13500 time, then the following will be the shares and earnings situation: 


|                     | owned shares | earnings/losses                                                      |
|---------------------|--------------|----------------------------------------------------------------------|
| owner               | 65           | 3.375 + 4.821428571428571 + 6.75 + (13500 x 0.01) = +149.94642857143 |
| investor 1 (yellow) | 5            | -3.375                                                               |
| investor 2 (red)    | 5            | -4.821428571428571                                                   |
| investor 3 (green)  | 5            | -6.75                                                                |
| MULTIVERSE                | 20           | 0                                                                    |

And so on, until the creator owns only one share, and so when BATMANISE will be downloaded 56700 times. At that point the shares and earnings situation will be the following: 

|                     | owned shares | earnings/losses                                                      |
|---------------------|--------------|----------------------------------------------------------------------|
| owner               | 1            | 3.375 + 4.821428571428571 + 6.75 + (56700 x 0.01) = +581.94642857143 |
| investor 1 (yellow) | 5            | -3.375                                                               |
| investor 2 (red)    | 5            | -4.821428571428571                                                   |
| investor 3 (green)  | 5            | -6.75                                                                |
| MULTIVERSE                | 84           | 0                                                                    |

Now, as long as people continue to install BATMANISE, investors will be paid out and their shares will be transferred to the MULTIVERSE. For instance, when BATMANISE is installed 2700 more times (and so when BATMANISE is installed 59400 times in total), the shares and earnings state will be the following:


|                     | owned shares | earnings/losses                                                      |
|---------------------|--------------|----------------------------------------------------------------------|
| owner               | 1            | 3.375 + 4.821428571428571 + 6.75 + (56700 x 0.01) = +581.94642857143 |
| investor 1 (yellow) | 5            | -3.375                                                               |
| investor 2 (red)    | 5            | -4.821428571428571                                                   |
| investor 3 (green)  | 1            | (2700 * 0.01) - 6.75 = +20.25                                        |
| MULTIVERSE                | 88           | 0                                                                    |

After another 2700 installation, shares of investor 3 will be completely transferred to the MULTIVERSE and the investor three will have reached the expected ROI of 27 AR. The process will continue until all the shares of investors are transferred to the MULTIVERSE, so in the end the earnings and shares situation will be the following: 

|                     | owned shares | earnings/losses                                                      |
|---------------------|--------------|----------------------------------------------------------------------|
| owner               | 1            | 3.375 + 4.821428571428571 + 6.75 + (56700 x 0.01) = +581.94642857143 |
| investor 1 (yellow) | 0            | (3375 * 0.01) - 3.375 = +30.375                                      |
| investor 2 (red)    | 0            | (3375 * 0.01) - 4.821428571428571 = + 28.92857142857                 |
| investor 3 (green)  | 0            | (3375 * 0.01) - 6.75 = +27                                           |
| MULTIVERSE                | 99           | 0                                                                    |

This means that, starting by this moment and forever, earnings will be split between the original owner (1%) and the users of BATMANISE (99%), and because the MULTIVERSE is a placeholder user that represents in each time a specific user, every time BATMANISE is installed 675 the MULTIVERSE will change. So, for instance, when BATMANISE is downloaded 675 times, the original owner will earn 0.0675 AR and the user represented by the MULTIVERSE will earn 6.6825 AR, and another user will raffled off the be the MULTIVERSE. Thus, for instance, if there are 1000 users of BATMANISE, each one of them will surely earn 6.6825 each time BATMANISE is downloaded 675000 times. 

The next sections describe the tools and software that OpenBits uses to handle all the processes described above.

## What OpenBits supplies? 

OpenBits supplies a Web Site and a Web Application ([http://openbits.world](http://openbits.world)), where you can publish OpenBits, where you can buy shares of other OpenBits and where you can check the situation of your OpenBits, of your shares and of your earnings.

Also, OpenBits supply a CLI that developers can use to install OpenBit (initially only Node packages) and later it will be improved in order to be used also to publish OpenBits ([https://www.npmjs.com/package/openbits](https://www.npmjs.com/package/openbits)). 


